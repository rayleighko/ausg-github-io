import React from 'react'

const Footer = ({ siteTitle }) => (
  <footer>
    <div className="container">
      <div className="info-area">
        <div className="column">
          <a href="https://github.com/ausg">GitHub</a>
          <a href="https://www.facebook.com/ausgkr/">Facebook</a>
        </div>
        <div className="column">
        {/*
          <a href="https://github.com/so-sc/code-of-conduct">Community Guidelines</a>
          <a href="https://github.com/so-sc/code-of-conduct">Code of Conduct</a>
        */}
        </div>
        <div className="column">
          <p>
            <strong>AUSG</strong><br/>
            Awskrug University Student Group<br/>
            ausg.awskrug@gmail.com<br/>
          </p>
        </div>
      </div>
    </div>
    <div className="copyright-area">
      <p>SOSC © { new Date().getFullYear() }</p>
    </div>
  </footer>
)

export default Footer
